# Use the base Maven image
FROM maven:3.8.8-eclipse-temurin-21-alpine

# Install PostgreSQL server, client, zip, curl, and create necessary directories with correct permissions
RUN apk add --no-cache postgresql postgresql-client zip curl && \
    getent group postgres || addgroup -S postgres && \
    getent passwd postgres || adduser -S postgres -G postgres && \
    mkdir -p /var/lib/postgresql/data /run/postgresql /home/postgres/.m2 && \
    chown -R postgres:postgres /var/lib/postgresql /run/postgresql /home/postgres/.m2 && \
    chmod 775 /run/postgresql /home/postgres/.m2

# Install required dependencies for SonarScanner's embedded JRE
RUN apk add --no-cache libc6-compat

# Install SonarScanner
RUN curl -sSLo /tmp/sonar-scanner.zip https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.7.0.2747-linux.zip && \
    unzip /tmp/sonar-scanner.zip -d /opt && \
    ln -s /opt/sonar-scanner-4.7.0.2747-linux/bin/sonar-scanner /usr/bin/sonar-scanner && \
    rm /tmp/sonar-scanner.zip

# Set environment variables for Maven configuration and repository location
ENV MAVEN_CONFIG="/home/postgres/.m2"
ENV MAVEN_OPTS="-Dmaven.repo.local=/home/postgres/.m2/repository"

# Switch to the PostgreSQL user
USER postgres

# Initialize the PostgreSQL database cluster
RUN initdb -D /var/lib/postgresql/data

# Expose the PostgreSQL port
EXPOSE 5432

# Set environment variables for PostgreSQL
ARG POSTGRES_PASSWORD=password

ENV POSTGRES_USER=admin
ENV POSTGRES_PASSWORD=$POSTGRES_PASSWORD
ENV POSTGRES_DB=spring-boot

# Start PostgreSQL and keep the container running
CMD ["sh", "-c", "pg_ctl start -D /var/lib/postgresql/data -l /var/lib/postgresql/data/logfile && tail -f /var/lib/postgresql/data/logfile"]
