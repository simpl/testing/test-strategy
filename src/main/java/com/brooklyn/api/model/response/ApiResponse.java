package com.brooklyn.api.model.response;

/**
 * Marker interface for response types.
 *
 * <p>This interface is used to mark classes that represent responses in the API. By implementing
 * this interface, a class is identified as a valid response type that can be returned from
 * controller methods in the API.
 *
 * <p>Implementing this interface does not require adding any methods; it simply serves as a way to
 * group response types together for type safety and clarity in the API's design.
 *
 * <p>Example classes that might implement this interface include:
 *
 * <ul>
 *   <li>{@code SuccessResponse} - Represents a successful operation response.
 *   <li>{@code ErrorResponse} - Represents an error response with a message and error code.
 *   <li>{@code DataResponse<T>} - Represents a response containing data of a specific type.
 * </ul>
 */
public interface ApiResponse {}
