package com.brooklyn.api.model.response;

import com.brooklyn.api.model.User;
import java.util.List;

/** Represents a response containing a list of users. Implements the ApiResponse interface. */
public class UserListResponse implements ApiResponse {
  private List<User> users;

  /**
   * Constructs a new UserListResponse object with the given list of users.
   *
   * @param users the list of users
   */
  public UserListResponse(List<User> users) {
    this.users = users;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }
}
