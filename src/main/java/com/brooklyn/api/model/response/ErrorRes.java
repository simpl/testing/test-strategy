package com.brooklyn.api.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * Represents an error response in the API.
 *
 * <p>This class is used to encapsulate error information, including the HTTP status code and an
 * error message. It implements the {@link ApiResponse} interface, making it a valid response type
 * for API endpoints.
 *
 * <p>This class leverages Lombok annotations for generating boilerplate code such as constructors,
 * getters, setters, and the {@code toString()} method.
 *
 * <p>Example usage:
 *
 * <pre>{@code
 * ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, "Invalid request data");
 * return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
 * }</pre>
 *
 * <p>Fields:
 *
 * <ul>
 *   <li>{@code status} - The HTTP status associated with the error.
 *   <li>{@code message} - A descriptive message providing details about the error.
 * </ul>
 *
 * @see ApiResponse
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorRes implements ApiResponse {

  /** The HTTP status associated with the error. */
  private HttpStatus status;

  /** A descriptive message providing details about the error. */
  private String message;
}
