package com.brooklyn.api.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents the request data for user registration.
 *
 * <p>This class is used to encapsulate the registration details (email, password, first name, and
 * last name) provided by the user during the registration process.
 *
 * <p>Example usage:
 *
 * <pre>{@code
 * RegistrationReq registrationRequest = new RegistrationReq(
 *         "user@example.com",
 *         "password123",
 *         "John",
 *         "Doe");
 * }</pre>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationReq {

  /** The email of the user registering an account. */
  private String email;

  /** The password of the user registering an account. */
  private String password;

  /** The first name of the user registering an account. */
  private String firstName;

  /** The last name of the user registering an account. */
  private String lastName;
}
