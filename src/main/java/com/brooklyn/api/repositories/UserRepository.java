package com.brooklyn.api.repositories;

import com.brooklyn.api.model.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for managing User entities. This interface extends JpaRepository, providing
 * basic CRUD operations, and defines custom queries to retrieve users based on specific criteria.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  /**
   * Retrieves a user by their email address.
   *
   * @param email The email address of the user to be retrieved.
   * @return The User entity associated with the specified email, or null if no user is found.
   */
  @Query("SELECT u FROM User u WHERE u.email = :email")
  Optional<User> findUserByEmail(@Param("email") String email);
}
