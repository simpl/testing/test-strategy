package com.brooklyn.api.controllers;

import com.brooklyn.api.auth.JwtUtil;
import com.brooklyn.api.model.User;
import com.brooklyn.api.model.request.LoginReq;
import com.brooklyn.api.model.request.RegistrationReq;
import com.brooklyn.api.model.response.ApiResponse;
import com.brooklyn.api.model.response.ErrorRes;
import com.brooklyn.api.model.response.LoginRes;
import com.brooklyn.api.model.response.UserListResponse;
import com.brooklyn.api.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional; // Add this line to import Optional
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** The AuthController class handles authentication and user registration endpoints. */
@RestController
@RequestMapping("/rest/auth")
public class AuthController {

  private final AuthenticationManager authenticationManager;
  private final BCryptPasswordEncoder bcryptPasswordEncoder;
  private final UserService userService;
  private final JwtUtil jwtUtil;

  /**
   * AuthController class is responsible for handling authentication-related requests. It provides
   * methods for user authentication and token generation.
   *
   * @param authenticationManager The authentication manager used for user authentication.
   * @param bcryptPasswordEncoder The BCrypt password encoder used for password hashing.
   * @param userService The UserService instance used for user-related operations.
   * @param jwtUtil The JwtUtil instance used for JWT token generation and validation.
   */
  public AuthController(
      AuthenticationManager authenticationManager,
      BCryptPasswordEncoder bcryptPasswordEncoder,
      UserService userService,
      JwtUtil jwtUtil) {
    this.authenticationManager = authenticationManager;
    this.bcryptPasswordEncoder = bcryptPasswordEncoder;
    this.userService = userService;
    this.jwtUtil = jwtUtil;
  }

  /**
   * Authenticates a user's login credentials and generates a JWT token upon successful
   * authentication.
   *
   * @param loginReq The login request object containing the user's email and password.
   * @return A ResponseEntity containing the API response with the login result.
   */
  @PostMapping("/login")
  public ResponseEntity<ApiResponse> login(@RequestBody LoginReq loginReq) {
    try {
      Authentication authentication =
          authenticationManager.authenticate(
              new UsernamePasswordAuthenticationToken(loginReq.getEmail(), loginReq.getPassword()));
      String email = authentication.getName();
      User user = new User(email, "");
      String token = jwtUtil.createToken(user);
      LoginRes loginRes = new LoginRes(email, token);

      return ResponseEntity.ok((ApiResponse) loginRes);
    } catch (BadCredentialsException e) {
      ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, "Invalid username or password");
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    } catch (Exception e) {
      ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }
  }

  /**
   * Registers a new user.
   *
   * @param request the HttpServletRequest object
   * @param response the HttpServletResponse object
   * @param registrationReq the RegistrationReq object containing the user's registration details
   * @return a ResponseEntity containing the ApiResponse object
   */
  @PostMapping("/register")
  public ResponseEntity<ApiResponse> register(
      HttpServletRequest request,
      HttpServletResponse response,
      @RequestBody RegistrationReq registrationReq) {
    try {
      // Check if a user with the given email already exists
      Optional<User> existingUser = userService.getUserByEmail(registrationReq.getEmail());
      if (existingUser.isPresent()) {
        ErrorRes errorResponse =
            new ErrorRes(HttpStatus.BAD_REQUEST, "User with this email already exists");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
      }

      // Proceed with user registration if no existing user is found
      User user = new User();
      user.setEmail(registrationReq.getEmail());
      user.setFirstName(registrationReq.getFirstName());
      user.setLastName(registrationReq.getLastName());
      user.setPassword(bcryptPasswordEncoder.encode(registrationReq.getPassword()));
      user.setRole("USER");

      User newUser = userService.createUser(user);
      if (newUser == null) {
        ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, "Error creating new user");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
      }

      newUser.setPassword("");
      String token = jwtUtil.createToken(newUser);
      LoginRes loginRes = new LoginRes(newUser.getEmail(), token);

      return ResponseEntity.ok((ApiResponse) loginRes);
    } catch (Exception e) {
      ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }
  }

  /**
   * Deletes a user by their ID.
   *
   * @param id The ID of the user to be deleted.
   * @return A ResponseEntity containing an ApiResponse object. If the user is successfully deleted,
   *     the response will have a status code of 200 OK. If the user is not found or could not be
   *     deleted, the response will have a status code of 400 Bad Request and an ErrorRes object
   *     with an appropriate error message.
   */
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<ApiResponse> deleteUser(@PathVariable("id") Long id) {
    try {
      boolean deleted = userService.deleteUserById(id);
      if (!deleted) {
        ErrorRes errorResponse =
            new ErrorRes(HttpStatus.BAD_REQUEST, "User not found or could not be deleted");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
      }
      return ResponseEntity.ok().body(new ApiResponse() {});
    } catch (Exception e) {
      ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }
  }

  /**
   * Deletes all users.
   *
   * @return A ResponseEntity with no content if the deletion is successful, or a ResponseEntity
   *     with a status of BAD_REQUEST if an exception occurs.
   */
  @DeleteMapping("/delete-all")
  public ResponseEntity<Void> deleteAllUsers() {
    try {
      userService.deleteAllUsers();
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
  }

  /**
   * Retrieves all users.
   *
   * @return ResponseEntity containing ApiResponse with the list of users if successful, or ErrorRes
   *     with the error message if an exception occurs.
   */
  @PostMapping("/get-all")
  public ResponseEntity<ApiResponse> getAllUsers() {
    try {
      List<User> users = userService.getAllUsers();
      return ResponseEntity.ok(new UserListResponse(users));
    } catch (Exception e) {
      ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }
  }
}
