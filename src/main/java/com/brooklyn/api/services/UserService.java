package com.brooklyn.api.services;

import com.brooklyn.api.model.User;
import com.brooklyn.api.repositories.UserRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

/**
 * The UserService class is responsible for managing user-related operations. It interacts with the
 * UserRepository to perform CRUD operations on user data.
 */
@Service
public class UserService {

  private final UserRepository userRepository;

  /**
   * Constructor for UserService.
   *
   * @param userRepository The UserRepository used to interact with the user data.
   */
  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  /**
   * Retrieves a user by their email address.
   *
   * @param email The email address of the user to retrieve.
   * @return An Optional containing the User object associated with the given email, or an empty
   *     Optional if no user is found.
   */
  public Optional<User> getUserByEmail(String email) {
    return userRepository.findUserByEmail(email);
  }

  /**
   * Creates a new user.
   *
   * @param user the user object to be created
   * @return the newly created user object
   */
  public User createUser(User user) {
    User newUser = userRepository.save(user);
    userRepository.flush();
    return newUser;
  }

  /**
   * Deletes a user by their ID.
   *
   * @param id the ID of the user to be deleted
   * @return true if the user was successfully deleted, false otherwise
   */
  public boolean deleteUserById(Long id) {
    if (userRepository.existsById(id)) {
      userRepository.deleteById(id);
      return true;
    }
    return false;
  }

  /** Deletes all users from the database. */
  public void deleteAllUsers() {
    userRepository.deleteAll();
  }

  /**
   * Retrieves all users from the database.
   *
   * @return a list of User objects representing all users in the database.
   */
  public List<User> getAllUsers() {
    return userRepository.findAll();
  }
}
