package com.brooklyn.api.services;

import com.brooklyn.api.model.User;
import com.brooklyn.api.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Custom implementation of the {@link UserDetailsService} interface to load user-specific data.
 *
 * <p>This service is used by Spring Security to authenticate users and build the user's
 * authentication token.
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

  private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);
  private final UserRepository userRepository;

  /**
   * Constructs a new {@code CustomUserDetailsService} with the given {@code UserRepository}.
   *
   * @param userRepository the repository used to access user data
   */
  public CustomUserDetailsService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  /**
   * Locates the user based on the email address. This method is used by Spring Security during the
   * authentication process.
   *
   * @param email the email address identifying the user whose data is required.
   * @return a fully populated {@code UserDetails} object (never {@code null})
   * @throws UsernameNotFoundException if the user could not be found or the user has no granted
   *     authority
   */
  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    logger.info("Loading user with email: {}", email);

    // Handle Optional returned by the repository
    User user =
        userRepository
            .findUserByEmail(email)
            .orElseThrow(
                () -> {
                  logger.warn("User with email {} not found", email);
                  return new UsernameNotFoundException("Email not found: " + email);
                });

    // Define user roles (assuming all users have a "USER" role)
    String[] roles = {"USER"};

    UserDetails userDetails =
        org.springframework.security.core.userdetails.User.builder()
            .username(user.getEmail())
            .password(user.getPassword())
            .roles(roles)
            .build();

    logger.info("User found: {}, roles: {}", user.getEmail(), roles);

    return userDetails;
  }
}
