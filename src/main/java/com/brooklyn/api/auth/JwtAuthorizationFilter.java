package com.brooklyn.api.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * This filter is responsible for processing incoming HTTP requests to ensure they contain a valid
 * JWT token in the Authorization header. If the token is valid, the user's authentication details
 * are set in the SecurityContext.
 */
@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter {

  private static final Logger appLogger = LoggerFactory.getLogger(JwtAuthorizationFilter.class);

  private final String jwtSecret;

  /**
   * Constructor that injects the JWT secret key from application properties.
   *
   * @param jwtSecret The secret key used to sign and verify the JWT token.
   */
  public JwtAuthorizationFilter(@Value("${jwt.secret}") String jwtSecret) {
    this.jwtSecret = jwtSecret;
  }

  /**
   * Filters each incoming request to validate the JWT token. If the token is valid, the user's
   * authentication details are stored in the SecurityContext.
   *
   * @param request The incoming HTTP request.
   * @param response The outgoing HTTP response.
   * @param filterChain The filter chain to pass the request/response to the next filter.
   * @throws ServletException If an error occurs during request processing.
   * @throws IOException If an input or output error is detected when the filter handles the
   *     request.
   */
  @Override
  protected void doFilterInternal(
      @NonNull HttpServletRequest request,
      @NonNull HttpServletResponse response,
      @NonNull FilterChain filterChain)
      throws ServletException, IOException {
    appLogger.info("Processing request: {}", request.getRequestURI());

    String token = request.getHeader("Authorization");

    if (token == null || !token.startsWith("Bearer ")) {
      appLogger.warn("No JWT token found in the request header");
      filterChain.doFilter(request, response);
      return;
    }

    try {
      String jwtToken = token.replace("Bearer ", "").trim();
      Claims claims =
          Jwts.parser()
              .setSigningKey(jwtSecret.getBytes()) // Set the secret key for signing
              .parseClaimsJws(jwtToken) // Parse the JWT token and extract claims
              .getBody();

      String email = claims.getSubject(); // Extract the subject (email) from claims
      appLogger.info("JWT token parsed successfully, email: {}", email);

      if (email != null && SecurityContextHolder.getContext().getAuthentication() == null) {
        UsernamePasswordAuthenticationToken authentication =
            new UsernamePasswordAuthenticationToken(email, null, new ArrayList<>());
        SecurityContextHolder.getContext().setAuthentication(authentication);
      }
    } catch (Exception e) {
      appLogger.error("Error processing JWT token", e);

      // Send error response if token validation fails
      Map<String, String> errorDetails = new HashMap<>();
      errorDetails.put("message", "Authentication Error");
      errorDetails.put("details", e.getMessage());
      response.setStatus(HttpStatus.FORBIDDEN.value());
      response.setContentType(MediaType.APPLICATION_JSON_VALUE);

      new ObjectMapper().writeValue(response.getWriter(), errorDetails);

      return; // Return immediately after sending the error response
    }

    filterChain.doFilter(request, response); // Proceed with the next filter in the chain
  }
}
