package com.brooklyn.api.auth;

import com.brooklyn.api.services.CustomUserDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/** Configuration class for security settings in the application. */
@Configuration
@EnableWebSecurity
public class SecurityConfig {

  private final CustomUserDetailsService userDetailsService;
  private final JwtAuthorizationFilter jwtAuthorizationFilter;

  /**
   * Security configuration class for handling authentication and authorization.
   *
   * @param customUserDetailsService The custom user details service used for user authentication.
   * @param jwtAuthorizationFilter The JWT authorization filter used for user authorization.
   */
  public SecurityConfig(
      CustomUserDetailsService customUserDetailsService,
      JwtAuthorizationFilter jwtAuthorizationFilter) {
    this.userDetailsService = customUserDetailsService;
    this.jwtAuthorizationFilter = jwtAuthorizationFilter;
  }

  /**
   * Configures the security filter chain for the HTTP requests.
   *
   * @param http the HttpSecurity object to configure
   * @return the configured SecurityFilterChain
   * @throws Exception if an error occurs during configuration
   */
  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    http.csrf(csrf -> csrf.disable()) // Disable CSRF protection
        .authorizeHttpRequests(
            authorizeRequests ->
                authorizeRequests
                    .requestMatchers("/rest/auth/**")
                    .permitAll()
                    .requestMatchers("/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html")
                    .permitAll() // Allow access to Swagger UI
                    .requestMatchers("/rest/story/**")
                    .permitAll()
                    .anyRequest()
                    .authenticated())
        .sessionManagement(
            sessionManagement ->
                sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
        .addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class);

    return http.build();
  }

  /**
   * Creates and configures an AuthenticationManager instance.
   *
   * @param http The HttpSecurity object used for configuring security settings.
   * @param bcryptPasswordEncoder The BCryptPasswordEncoder object used for password encoding.
   * @return The configured AuthenticationManager instance.
   * @throws Exception If an error occurs during configuration.
   */
  @Bean
  public AuthenticationManager authenticationManager(
      HttpSecurity http, BCryptPasswordEncoder bcryptPasswordEncoder) throws Exception {
    AuthenticationManagerBuilder authenticationManagerBuilder =
        http.getSharedObject(AuthenticationManagerBuilder.class);
    authenticationManagerBuilder
        .userDetailsService(userDetailsService)
        .passwordEncoder(bcryptPasswordEncoder);
    return authenticationManagerBuilder.build();
  }

  /**
   * Returns a BCryptPasswordEncoder object.
   *
   * @return the BCryptPasswordEncoder object.
   */
  @Bean
  public BCryptPasswordEncoder bcryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
