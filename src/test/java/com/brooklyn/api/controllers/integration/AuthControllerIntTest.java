package com.brooklyn.api.controllers.integration;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.brooklyn.api.model.User;
import com.brooklyn.api.model.request.LoginReq;
import com.brooklyn.api.model.request.RegistrationReq;
import com.brooklyn.api.repositories.UserRepository;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

/**
 * Integration test class for the AuthController.
 *
 * <p>This class contains integration tests for the registration, login, and user management
 * endpoints of the AuthController. It uses the SpringBootTest annotation to start the application
 * in a random port and the ActiveProfiles annotation to use the "test" profile. The class also
 * defines constants for the test email and password.
 *
 * <p>The class includes the following test methods: - testRegisterSuccess: Tests the successful
 * registration of a user. - testLoginSuccess: Tests a successful login. - testGetAllUsers: Tests
 * the retrieval of all users (disabled). - testDeleteAllUsers: Tests the deletion of all users
 * (disabled). - testLoginFailure: Tests a failed login (disabled). - testRegisterFailure: Tests a
 * failed registration (disabled).
 *
 * <p>The class also includes the following setup methods: - cleanDatabase: Cleans the database by
 * deleting all stories and users before each test case. - setUp: Sets up the test environment
 * before each test case by setting the RestAssured port.
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class AuthControllerIntTest {

  @LocalServerPort private int port;

  @Autowired private UserRepository userRepository;

  // Define constants for email and password
  private static final String TEST_EMAIL = "x@brooklyn.com";
  private static final String TEST_PASSWORD = "12345";

  /** Set up the test environment before each test case. */
  @BeforeEach
  public void setUp() {
    RestAssured.port = port;
  }

  /**
   * Test case for successful user registration.
   *
   * <p>This test verifies that a user can successfully register by sending a POST request to the
   * "/rest/auth/register" endpoint. The test sends a registration request with the specified email,
   * password, title, and name. It then asserts that the response status code is 200 (OK), and that
   * the response body contains the registered email and a non-null token.
   *
   * <p>Test Steps: 1. Register User: - Send a POST request to "/rest/auth/register" with the
   * registration request body. - Assert that the response status code is 200 (OK). - Assert that
   * the response body contains the registered email and a non-null token.
   */
  @Test
  @Order(1)
  @Step("Step 1: Register User")
  void testRegisterSuccess() {
    RegistrationReq registrationReq = new RegistrationReq(TEST_EMAIL, TEST_PASSWORD, "Mr.", "x");

    // ensure prior test run the DB is empty
    userRepository.deleteAll();

    given()
        .filter(new AllureRestAssured())
        .contentType(ContentType.JSON)
        .body(registrationReq)
        .when()
        .post("/rest/auth/register")
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("email", equalTo(TEST_EMAIL))
        .body("token", notNullValue());

    // Verify that the user is actually saved in the database
    Optional<User> savedUser = userRepository.findUserByEmail(TEST_EMAIL);
    assertTrue(savedUser.isPresent(), "User should be saved in the database");
  }

  /**
   * Test a successful login.
   *
   * <p>This test sends a POST request to the "/rest/auth/login" endpoint with valid credentials and
   * checks that the response contains the expected email and a valid JWT token.
   */
  @Test
  @Order(2)
  @Step("Step 2: Login success")
  void testLoginSuccess() {
    LoginReq loginReq = new LoginReq(TEST_EMAIL, TEST_PASSWORD);

    // Ensure user exists in the database before attempting login
    Optional<User> registeredUser = userRepository.findUserByEmail(TEST_EMAIL);
    assertTrue(registeredUser.isPresent(), "User should exist in the database before login test");

    // Log information about the user retrieved
    System.out.println("User retrieved from DB: " + registeredUser.get());

    given()
        .log()
        .all() // Log request details
        .contentType(ContentType.JSON)
        .body(loginReq)
        .when()
        .post("/rest/auth/login")
        .then()
        .log()
        .all() // Log response details
        .statusCode(HttpStatus.OK.value()) // Expecting 200 OK
        .contentType(ContentType.JSON)
        .body("email", equalTo(TEST_EMAIL))
        .body("token", notNullValue());
  }

  /**
   * Test case for getting all users.
   *
   * <p>This test case verifies the functionality of the "Get All Users" API endpoint. It sends a
   * POST request to the "/rest/auth/get-all" endpoint and expects a response with HTTP status code
   * 200 (OK). The response should have content type JSON and the "users" field should not be null.
   *
   * <p>This test case is currently disabled as it is still in development.
   *
   * @see com.brooklyn.api.controllers.integration.AuthControllerIntTest
   */
  @Test
  @Order(3)
  @Step("Step 3: Get All Users")
  void testGetAllUsers() {
    given()
        .filter(new AllureRestAssured())
        .when()
        .post("/rest/auth/get-all")
        .then()
        .statusCode(HttpStatus.OK.value())
        .contentType(ContentType.JSON)
        .body("users", notNullValue());
  }

  /**
   * Test case for deleting all users.
   *
   * <p>This test case verifies the functionality of deleting all users through the
   * "/rest/auth/delete-all" endpoint. It sends a DELETE request to the endpoint and expects a 200
   * OK status code in response.
   *
   * <p>This test is currently disabled as it is still in development.
   *
   * <p>Test order: 4 Test step: Step 1: Delete All Users
   */
  @Test
  @Order(4)
  @Step("Step 4: Delete All Users")
  void testDeleteAllUsers() {
    given()
        .filter(new AllureRestAssured())
        .contentType(ContentType.JSON)
        .when()
        .delete("/rest/auth/delete-all")
        .then()
        .statusCode(HttpStatus.OK.value());
  }

  /**
   * Test case for login failure.
   *
   * <p>This test verifies the behavior of the login functionality when the provided credentials are
   * incorrect. It sends a POST request to the "/rest/auth/login" endpoint with an invalid email and
   * password combination. The expected behavior is that the server responds with a 400 Bad Request
   * status code and an error message indicating invalid username or password.
   */
  @Test
  @Order(5)
  @Step("Step 5: Login failed")
  void testLoginFailure() {
    LoginReq loginReq = new LoginReq("test@example.com", "wrongpassword");

    given()
        .contentType(ContentType.JSON)
        .body(loginReq)
        .when()
        .post("/rest/auth/login")
        .then()
        .statusCode(HttpStatus.BAD_REQUEST.value()) // Ensure it returns 400 status code
        .body("status", equalTo("BAD_REQUEST")) // Expect just "BAD_REQUEST"
        .body("message", equalTo("Invalid username or password"));
  }

  /**
   * Test case for registering a user with invalid data.
   *
   * <p>This test verifies that when a user tries to register with invalid data, the API returns a
   * bad request status code and an appropriate error message.
   *
   * <p>This test is currently disabled as it is still in development.
   *
   * @see com.brooklyn.api.controllers.integration.AuthControllerIntTest
   */
  @Test
  @Order(6)
  @Step("Step 6: Register failure")
  void testRegisterFailure() {
    // Ensure the user exists in the database before the test
    User existingUser = new User();
    existingUser.setEmail("existinguser@example.com");
    existingUser.setPassword("password");
    existingUser.setFirstName("John");
    existingUser.setLastName("Doe");
    existingUser.setRole("USER");
    userRepository.save(existingUser);
    userRepository.flush();

    RegistrationReq registrationReq =
        new RegistrationReq("existinguser@example.com", "password", "John", "Doe");

    given()
        .contentType(ContentType.JSON)
        .body(registrationReq)
        .when()
        .post("/rest/auth/register")
        .then()
        .statusCode(HttpStatus.BAD_REQUEST.value())
        .body("status", equalTo("BAD_REQUEST"))
        .body("message", equalTo("User with this email already exists"));
  }
}
