#!/bin/bash


echo "clean build started ..."
./mvnw clean install -DskipTests -e

echo "running tests ..."
./mvnw test

echo "creating reports ..."
./mvnw allure:report 

echo "doing the code check and create report..."
./mvnw jxr:jxr jxr:test-jxr checkstyle:checkstyle

echo "starting report server ..."   
allure serve -h localhost